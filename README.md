
This is a copy of the pdolsms package by Raad Khraishi obtained from the archived version on CRAN: 
https://cran.r-project.org/src/contrib/Archive/pdolsms/

The purpose of this package is to estimate the relationship between cointegrated vectors 
in a panel context by Dynamic Ordinary Least Squares (DOLS) folling a method developped 
by Mark and Sul (2003).


